FROM fedora:31 AS build
RUN dnf install -y cmake ninja-build git gcc-c++
RUN git clone --depth=1 https://github.com/yrnkrn/zapcc llvm
RUN cmake -Bbuild -Hllvm -GNinja -DCMAKE_BUILD_TYPE=Release -DLLVM_ENABLE_WARNINGS=OFF
RUN ninja -Cbuild install

FROM registry.fedoraproject.org/fedora-minimal:31
COPY --from=build /usr/local /usr/local
RUN microdnf install gcc-c++ && microdnf clean all
ENV CC zapcc
ENV CXX zapcc++
